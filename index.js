console.log("hi there");

function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0) {
            timer = duration;
        }
    }, 1000);
}

window.onload = function () {
    var threeMinutes = 60 * 3,
        display = document.querySelector('#time');
    startTimer(threeMinutes, display);
};

setTimeout(endTimer, 181500);

function endTimer(){
    checkAnswers();
    alert("Time is Up!");
}

function checkAnswers() {
  var score = 0;
  var wrong1 = "Correct answer for Q1 is A";
  var wrong2 = "Correct answer for Q2 is B";
  var wrong3 = "Correct answer for Q3 is D";
  var wrong4 = "Correct answer for Q4 is B";
  var wrong5 = "Correct answer for Q5 is C";
  
  if (document.getElementById("1a").checked) {
    score += 2;
    document.getElementById("wrong1").innerHTML = "";
  } else {
    document.getElementById("wrong1").innerHTML = wrong1;
  }

  if (document.getElementById("2b").checked) {
    score += 2;
    document.getElementById("wrong2").innerHTML = "";
  } else {
    document.getElementById("wrong2").innerHTML = wrong2;
  }

  if (document.getElementById("3d").checked) {
    score += 2;
    document.getElementById("wrong3").innerHTML = "";
  } else {
    document.getElementById("wrong3").innerHTML = wrong3;
  }

  if (document.getElementById("4b").checked) {
    score += 2;
    document.getElementById("wrong4").innerHTML = "";
  } else {
    document.getElementById("wrong4").innerHTML = wrong4;
  }

  if (document.getElementById("5c").checked) {
    score += 2;
    document.getElementById("wrong5").innerHTML = "";
  } else {
    document.getElementById("wrong5").innerHTML = wrong5;
  }



  document.getElementById("score").innerHTML = "Score is: " + score + "/10";
}
